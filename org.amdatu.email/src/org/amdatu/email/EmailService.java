/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email;

/**
 * Platform-agnostic service interface for sending email.
 */
public interface EmailService {

    /**
     * Send an email message. May throw {@link EmailException} if message cannot be sent.
     * 
     * @param message the message to be sent
     */
    void send(Message message);

}
