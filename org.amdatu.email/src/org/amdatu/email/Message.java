/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents an email message to be sent. Construct a new {@link Message} using the
 * {@link Builder} provided by this class.
 */
public class Message {
    private final Set<String> m_recipients;
    private final String m_htmlBody;
    private final String m_textBody;
    private final String m_from;
    private final String m_fromFriendlyName;
    private final String m_subject;

    /**
     * Create a new email message.
     * 
     * @param recipients non-empty set of recipient email addresses
     * @param from email address used as from-address
     * @param subject subject line of the email
     * @param htmlBody html representation of the email body
     * @param textBody plaintext representation of the email body
     */
    public Message(Set<String> recipients, String from, String subject, String htmlBody, String textBody) {
    	this(recipients, from, null, subject, htmlBody, textBody);
    }

    /**
     * Create a new email message.
     * 
     * @param recipients non-empty set of recipient email addresses
     * @param from email address used as from-address
     * @param fromFriendlyName the friendly name for the sender
     * @param subject subject line of the email
     * @param htmlBody html representation of the email body
     * @param textBody plaintext representation of the email body
     */
    public Message(Set<String> recipients, String from, String fromFriendlyName, String subject, String htmlBody, String textBody) {
        if (recipients == null || recipients.isEmpty()) {
            throw new IllegalArgumentException("Message must have at least on recipient");
        }

        m_recipients = recipients;
        m_from = from;
        m_fromFriendlyName = fromFriendlyName;
        m_subject = subject;
        m_htmlBody = htmlBody;
        m_textBody = textBody;
    }
    
    public Set<String> getRecipients() {
        return m_recipients;
    }

    public String getHtmlBody() {
        return m_htmlBody;
    }

    public String getTextBody() {
        return m_textBody;
    }

    public String getFrom() {
        return m_from;
    }

    public String getFromFriendlyName() {
        return m_fromFriendlyName;
    }
    
    /**
     * Return the full email from sender. This is formatted according to RFC-822 section 6.
     * It will include the friendly name if there is one.
     * 
     * @return the full email from header
     */
    public String getFormattedFrom() {
    	if (m_fromFriendlyName == null) {
    		return m_from;
    	} else {
    		StringBuilder sb = new StringBuilder();
    		sb.append(m_fromFriendlyName);
    		sb.append(" <");
    		sb.append(m_from);
    		sb.append(">");
    		return sb.toString();
    	}
    }

    public String getSubject() {
        return m_subject;
    }

    @Override
    public String toString() {
        return "Message [m_recipients=" + m_recipients + ", m_htmlBody=" + m_htmlBody + ", m_textBody=" + m_textBody
            + ", m_from=" + m_from + ", m_fromFriendlyName=" + m_fromFriendlyName + ", m_subject=" + m_subject + "]";
    }

    public static class Builder {
        private Set<String> m_recipients = new HashSet<>();
        private String m_htmlBody;
        private String m_textBody;
        private String m_from;
        private String m_fromFriendlyName;
        private String m_subject;

        public static Builder create() {
            return new Builder();
        }

        public Builder recipient(String... recipient) {
            m_recipients.addAll(Arrays.asList(recipient));
            return this;
        }

        public Builder recipients(Collection<String> recipients) {
            m_recipients.addAll(recipients);
            return this;
        }

        public Builder htmlBody(String htmlBody) {
            m_htmlBody = htmlBody;
            return this;
        }

        public Builder textBody(String textBody) {
            m_textBody = textBody;
            return this;
        }

        public Builder from(String from, String fromFriendlyName) {
            m_from = from;
            m_fromFriendlyName = fromFriendlyName;
            return this;
        }

        public Builder from(String from) {
            m_from = from;
            return this;
        }
        
        public Builder subject(String subject) {
            m_subject = subject;
            return this;
        }

        public Message build() {
    		return new Message(m_recipients, m_from, m_fromFriendlyName, m_subject, m_htmlBody, m_textBody);
        }
    }

}
