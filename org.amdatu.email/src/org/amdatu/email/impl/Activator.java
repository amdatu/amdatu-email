/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.impl;

import org.amdatu.email.EmailService;
import org.amdatu.email.EmailTransportService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext bc, DependencyManager dm) throws Exception {
        dm.add(createComponent().setInterface(EmailWhitelistService.class.getName(), null)
            .setImplementation(EmailWhitelistService.class).add(createServiceDependency().setService(LogService.class))
            .add(createConfigurationDependency().setPid("org.amdatu.email")));

        dm.add(createComponent().setInterface(EmailService.class.getName(), null)
            .setImplementation(EmailServiceImpl.class)
            .add(createServiceDependency().setService(EmailWhitelistService.class).setRequired(true))
            .add(createServiceDependency().setService(EmailTransportService.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class)));
    }

}
