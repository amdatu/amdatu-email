/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.impl;

import java.util.Set;

import org.amdatu.email.EmailService;
import org.amdatu.email.EmailTransportService;
import org.amdatu.email.Message;

public class EmailServiceImpl implements EmailService {

    private volatile EmailTransportService m_emailTransportService;
    private volatile EmailWhitelistService m_emailWhitelistService;

    @Override
    public void send(Message message) {
        Set<String> recipients = m_emailWhitelistService.filterRecipients(message.getRecipients());
        if (recipients.isEmpty()) {
            // Don't try and send a message if there is no one to receive it.
            return;
        }

        m_emailTransportService.send(new Message(recipients, message.getFrom(), message.getSubject(), message.getHtmlBody(), message.getTextBody()));
    }

}
