/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.impl;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

public class EmailWhitelistService implements ManagedService {
    private volatile LogService m_logService;
    private volatile List<Pattern> m_whitelist;

    public Set<String> filterRecipients(Set<String> recipients) {
        // Use a copy of the whitelist reference as it can be updated in the updated method
        final List<Pattern> whitelist = m_whitelist;
        if (whitelist == null || whitelist.isEmpty()) {
            // No need to check the whitelist as its non existent, just return all recipients
            return recipients;
        }
        return recipients.stream().filter(r -> {
            for (Pattern wl : whitelist) {
                if (wl.matcher(r).matches()) {
                    return true;
                }
            }
            if (m_logService != null) {
                m_logService.log(LogService.LOG_INFO,
                    String.format("Not sending email to '%s' as the email adress is not whitelisted.", r));
            }
            return false;
        }).collect(Collectors.toSet());
    }

    @Override
    public void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
        m_whitelist = new ArrayList<>();
        String whitelist = (String) properties.get("whitelist");
        if (whitelist == null) {
            throw new ConfigurationException("whitelist", "Required configuration property whitelist missing");
        }
        if (whitelist.length() > 0) {
            String[] whitelistArr = whitelist.split(";");
            // Trim off any spaces, that might occur in a example like: "@gmail.com; @other.com"
            for (int i = 0; i < whitelistArr.length; i++) {
                m_whitelist.add(Pattern.compile(whitelistArr[i].trim()));
            }
        }
    }
}
