/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;

public class EmailWhitelistServiceTest {
    private final EmailWhitelistService m_emailWhitelistService = new EmailWhitelistService();
    private final Set<String> m_recipients = new HashSet<>(
        Arrays.asList("test@mail.com", "other@mail.com", "part@else.nl", "nl@else.org"));

    @Test(expected = ConfigurationException.class)
    public void shouldNotFilteroutOnNullWhitelist() throws ConfigurationException {
        setWhitelist(null);
    }

    @Test
    public void shouldNotFilteroutOnEmptyWhitelist() throws ConfigurationException {
        setWhitelist("");
        Set<String> filtered = m_emailWhitelistService.filterRecipients(m_recipients);
        assertEquals("All recipients should remain", m_recipients.size(), filtered.size());
    }

    @Test
    public void shouldFilteroutNonMailDotCom() throws ConfigurationException {
        setWhitelist(".*@mail.com");
        Set<String> filtered = m_emailWhitelistService.filterRecipients(m_recipients);
        assertEquals("All recipients with @mail.com should remain", 2, filtered.size());
    }

    @Test
    public void shouldFilteroutNonMailDotComWith2Whitelists() throws ConfigurationException {
        setWhitelist(".*@mail\\.com; \\.com");
        Set<String> filtered = m_emailWhitelistService.filterRecipients(m_recipients);
        assertEquals("All recipients with @mail.com should remain", 2, filtered.size());
    }

    @Test
    public void shouldFilteroutFullyQualifiedEmail() throws ConfigurationException {
        setWhitelist(m_recipients.iterator().next());
        Set<String> filtered = m_emailWhitelistService.filterRecipients(m_recipients);
        assertEquals(1, filtered.size());
    }

    private void setWhitelist(String whitelist) throws ConfigurationException {
        Properties properties = new Properties();
        if (whitelist != null) {
            properties.setProperty("whitelist", whitelist);
        }
        m_emailWhitelistService.updated(properties);
    }

}
