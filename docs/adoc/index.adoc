:toc: left
:toclevels: 3
:toc-title: Amdatu Email
:homepage: https://www.amdatu.org/email
:source-highlighter: coderay
:icons: font
:docinfo: shared
:imagesdir: images

= Amdatu Email

include::_includes/intro.adoc[]

include::_includes/how-to-use.adoc[]

include::_includes/components.adoc[]

include::_includes/dependencies.adoc[]

include::_includes/resources.adoc[]
