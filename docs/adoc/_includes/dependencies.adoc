== Dependencies

The following table provides an overview of the required and optional dependencies for Amdatu Email:

[cols="40%,10%,50%"]
|===
| Bundle
	| Required
	| Description
| org.apache.felix.dependencymanager
	| yes
	| Apache Felix Dependency Manager is used internally by all Amdatu components
| Implementation of Config Admin (e.g. org.apache.felix.configadmin)
	| yes
	| Used to configure data sources
| OSGi LogService (e.g. Apache Felix LogService)
	| no
	| Used for logging purposes
|===

Even though you are free to provide your own implementations, all dependencies (as the given examples where applicable) can be resolved from the Amdatu dependencies repository.

=== System package dependencies

*Please note that the EmailServices depend on various system packages:*

The lines below can be used to resolve the system package dependencies required when running the corresponding EmailService implementation:

*SMTP email bundle:* `-runsystempackages: sun.security.util`



