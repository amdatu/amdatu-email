== Components

Amdatu Email provides the following components:

[cols="40%,10%,50%"]
|===
| Bundle
	| Required
	| Description
| org.amdatu.email.api
	| yes
	| Provides the EmailService API
| org.amdatu.email.impl
	| yes
	| Provides the EmailService implementation
| org.amdatu.email.smtp
	| no*
	| Provides an implementation to send email through a configured SMTP server
| org.amdatu.email.mock
	| no*
	| Provides a mock implementation which outputs sent messages to the LogService
|===

[.small]#* At least one backend is required at runtime#

